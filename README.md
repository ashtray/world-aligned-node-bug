# World aligned node bug

bug.lua defines two nodes. They should both have black and white areas on them.

As shown in bug.png, one of them loses its white areas when it is made into a wieldmesh.

![bugged nodes losing their white overlay](bug.png "bug")
